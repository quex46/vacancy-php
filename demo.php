<?php
require 'vendor/autoload.php';

use TJ\Uploader\File;
use TJ\Uploader\Uploader;

define('AWS_KEY', '');
define('AWS_SECRET', '');
define('AWS_BUCKET', '');
define('AWS_REGION', '');

define('SELECTEL_LOGIN', '');
define('SELECTEL_PASSWORD', '');
define('SELECTEL_CONTAINER', '');

if (isset($_POST['upload'])) {
    $action = $_POST['upload'];
    $storage = intval($_POST['storage']);
    $uploader = new Uploader($storage, array(
        'aws_access_key_id' => AWS_KEY,
        'aws_secret_access_key' => AWS_SECRET,
        'aws_bucket' => AWS_BUCKET,
        'aws_region' => AWS_REGION,
        'selectel_user' => LOGIN,
        'selectel_password' => PASSWORD,
        'selectel_container' => SELECTEL_CONTAINER
    ));


    echo '<pre>';
    
    if ($action === 'by_file') {
        $result = $uploader->uploadFromFile($_FILES['file']);
        print_r($result->getData());
    } else if ($action === 'by_link') {
        $result = $uploader->uploadFromUrl($_POST['file']);
        print_r($result->getData());
    } else {
        echo 'No Data';
    }

    echo '</pre>';
   
    die();
}

?>
<style>
    td {
        padding: 1rem;
    }
    input[type="text"] {
        width: 600px;
    }
</style>
<table>
    <tr>
        <td>
            <h2>Загрузка файла</h2>
            <form method="POST" enctype="multipart/form-data">
                <input type="file" name="file">
                <button type="submit">Загрузка</button>
                <input type="hidden" name="upload" value="by_file">
                <br>
                <input type="radio" checked name="storage" value="<?= Uploader::AMAZON ?>"> Amazon
                <input type="radio" name="storage" value="<?= Uploader::SELECTEL ?>"> Selectel
            </form>
        </td>
    </tr>
    <tr>
        <td>
            <h2>Загрузить по ссылке</h2>
            <form method="POST" enctype="multipart/form-data">
                <input type="text" name="file" value="https://www.google.ru/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png">
                <button type="submit">Загрузка</button>
                <input type="hidden" name="upload" value="by_link">
                <br>
                <input type="radio" checked name="storage" value="<?= Uploader::AMAZON ?>"> Amazon
                <input type="radio" name="storage" value="<?= Uploader::SELECTEL ?>"> Selectel
            </form>
        </td>
    </tr>
</table>