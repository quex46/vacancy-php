<?php
namespace TJ\Uploader;

use GuzzleHttp\Client;
use Aws\S3\S3Client;
use Exception;
use SelectelStorage;

class Uploader implements UploaderInterface
{
    const AMAZON = 1;
    const SELECTEL = 2;

    private $storage;
    private $storageParams;

    public function __construct($storage, $storageAuthParameters)
    {
        $this->storage = $storage;
        $this->storageParams = array_merge(
            array(
                'aws_access_key_id' => null,
                'aws_secret_access_key' => null,
                'aws_region' => null,
                'aws_bucket' => null,
                'aws_acl' => 'public-read',
                'selectel_user' => null,
                'selectel_password' => null,
                'selectel_container' => null,
                'selectel_container_type' => 'public',
            ),
            $storageAuthParameters
        );
    }

    /**
     * Сохранение картинки во временный файл по url
     * @param  string  $url  Ссылка на файл
     * @return array         Информация о скачанной картинке
     *                       (массив идентичный элементу $_FILES)
     */
    private static function saveFromUrl($url)
    {
        $result = array(
            'name' => basename($url),
            'tmp_name' => null,
            'type' => null,
            'size' => null,
            'error' => UPLOAD_ERR_OK
        );

        $filename = $result['tmp_name'] = tempnam(sys_get_temp_dir(), 'tjo');
        
        if (!$filename) {
            $result['error'] = UPLOAD_ERR_CANT_WRITE;
            return $result;
        }

        $fh = @fopen($filename, 'w');
        
        if (!$fh) {
            $result['error'] = UPLOAD_ERR_CANT_WRITE;
            return $result;
        }

        $client = new Client();
        
        try {
            $resp = $client->get($url, array('sink' => $fh));
        } catch (Exception $e) {
            return $result;
        }        
        
        fclose($fh);

        $status = $resp->getStatusCode();
        
        if ($status !== 200) {
            $result['error'] = UPLOAD_ERR_NO_FILE;
            return $result;
        }

        $contentLength = $result['size'] = intval($resp->getHeaderLine('Content-Length'));
        $contentLengthMax = ini_get('upload_max_filesize');

        if ($contentLength > $contentLengthMax) {
            $result['error'] = UPLOAD_ERR_INI_SIZE;
            return $result;
        }

        $contentType = $result['type'] = $resp->getHeader('Content-Type');

        return $result;
    }


    /**
     * Загрузка картинки в облако
     * @param  File $file   Экземпляр File
     * @return File
     */
    private function upload($file)
    {
        if (substr($file->mime, 0, 6) !== 'image/') {
            return $file;
        }

        if ($this->storage === self::AMAZON) {
            $s3 = S3Client::factory(array(
                'credentials' => array(
                    'key' => $this->storageParams['aws_access_key_id'],
                    'secret' => $this->storageParams['aws_secret_access_key'],
                ),
                'region' => $this->storageParams['aws_region'],
                'version' => 'latest'                
            ));

            $result = $s3->putObject(array(
                'Bucket' => $this->storageParams['aws_bucket'],
                'ACL' => $this->storageParams['aws_acl'],                
                'SourceFile' => $file->tmp_name,
                'Key' => date('Y/m/d/').$file->name,
                'ContentType' => $file->mime
            ));

            $file->url = $result['ObjectURL'];
        } else if ($this->storage === self::SELECTEL) {            
            $ss = new SelectelStorage(
                $this->storageParams['selectel_user'],
                $this->storageParams['selectel_password']
            );

            $container = $ss->getContainer($this->storageParams['selectel_container']);
            $result = $container->putFile($file->tmp_name, date('Y/m/d/').$file->name);
            
            $file->url = $result['url'];
        }

        unlink($file->tmp_name);
        
        return $file;
    }

    public function uploadFromFile($uploadedFileData)
    {
        $file = new File($uploadedFileData);
        return $this->upload($file);
    }

    public function uploadFromUrl($url)
    {
        $uploadedFileData = self::saveFromUrl($url);
        return $this->uploadFromFile($uploadedFileData);
    }

}