<?php
namespace TJ\Uploader;

class File implements FileInterface
{
    public $width;      // Ширина
    public $height;     // Высота
    public $mime;       // mime-тип (image/*)
    public $size;       // Размер в байтах
    public $name;       // Название файла в облаке
    public $tmp_name;   // Временный файл из которого происходит загрузка в облако
    public $url;        // Ссылка на файл в облаке

    /**
     * @param array $uploadedFileData Файл из массива $_FILES
     */
    public function __construct($uploadedFileData = null)
    {
        $info = @getimagesize($uploadedFileData['tmp_name']);
        
        if (!empty($info)) {
            $this->tmp_name = $uploadedFileData['tmp_name'];
            $this->size = $uploadedFileData['size'];
            $this->width = $info[0];
            $this->height = $info[1];
            $this->mime = $info['mime'];                     
            // Формируем название по md5-хэшу файла, 
            // что потенциально может сэкономить нам место в облаке.
            $this->name = md5_file($uploadedFileData['tmp_name']).image_type_to_extension($info[2]);                    
        }        
    }

    public function getData()
    {
        return array(
            'width' => $this->width,
            'height' => $this->height,
            'mime' => $this->mime,
            'size' => $this->size,
            'name' => $this->name,
            'url' => $this->url
        );
    }

    public function getUrl()
    {
        return is_string($this->url) ? $this->url : '';
    }

    public function getSize()
    {
        return $this->size;
    }

    public function __toString()
    {
        return $this->getUrl();
    }

}